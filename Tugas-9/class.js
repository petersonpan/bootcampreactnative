class Animal {
  constructor(name,legs=4,cold_blooded = false) {
    this.name = name;
    this.legs = legs;
    this.cold_blooded = cold_blooded;
  }
  get jumkaki() {
    return this.legs;
  }
}


const sheep = new Animal("shaun");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded);
console.log(sheep.jumkaki);

class Ape extends Animal{
  constructor(name){
    super(name);
    this.name=name;
    this.legs=2;
    this.cold_blooded=false;
  }
  yell(){
    console.log("Auooo");
  }
}

class Frog extends Animal{
  constructor(name){
    super(name);
    this.name=name;
    this.legs=4;
    this.cold_blooded=true;
  }
  jump(){
    console.log("hop hop");
  }
}

const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell();

const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"

class Animal {
  constructor(name,legs=4,cold_blooded = false) {
    this.name = name;
    this.legs = legs;
    this.cold_blooded = cold_blooded;
  }
  jumkaki() {
    return this.legs;
  }
}


const sheep = new Animal("shaun");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded);
console.log(sheep.jumkaki());

class Ape extends Animal{
  constructor(name){
    super(name);
    this.name=name;
    this.legs=2;
    this.cold_blooded=true;
  }
   yell(){
    console.log("Auooo");
  }
}

const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell;

class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

const clock =  new Clock({template: 'h:m:s' });
clock.start();
