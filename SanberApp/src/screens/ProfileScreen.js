import React from "react";
import { Title, Text } from "react-native-paper";
import { View, StyleSheet, SafeAreaView, Image } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { FontAwesome, AntDesign } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
import { AuthContext } from "../providers/AuthProvider";
import HeaderHome from "./../components/Header";

const ProfileScreen = (props) => {
  console.log("Props:"+props);
  
  return (
    <AuthContext.Consumer>
      {(auth) => (
        <SafeAreaView style={styles.container}>
          <HeaderHome
            DrawerFunction={() => {
              props.navigation.toggleDrawer();
            }}
          />
          <View style={styles.userInfoSection}>
            <View style={styles.row}>
              <MaterialIcons
                name="face-retouching-natural"
                size={24}
                color="#777777"
              />
              <Text style={styles.textList}>
                {auth.CurrentUser.displayName}
              </Text>
            </View>

            <View style={styles.row}>
              <FontAwesome name="birthday-cake" size={20} color="#777777" />
              <Text style={styles.textList}>
                Born on 29 February, 1994{" "}
              </Text>
            </View>
            <View style={styles.row}>
              <Icon name="map-marker-radius" color="#777777" size={20} />
              <Text style={styles.textList}>
                Jakarta, DKI Jakarta
              </Text>
            </View>
            <View style={styles.row}>
              <Icon name="phone" color="#777777" size={20} />
              <Text style={styles.textList}>
              {auth.CurrentUser.uid}
              </Text>
            </View>
            <View style={styles.row}>
              <Icon name="email" color="#777777" size={20} />
              <Text style={styles.textList}>
                {auth.CurrentUser.email}
              </Text>
            </View>
          </View>
        </SafeAreaView>
      )}
    </AuthContext.Consumer>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 20,
    color: "blue",
    marginTop: 5,
    textAlign: "left",
    paddingLeft: 30,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginTop: 50,
  },
  container: {
    flex: 1,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  textList:
    { color: "#777777", marginLeft: 20, fontSize: 15 }
  ,
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: "500",
  },
  row: {
    flexDirection: "row",
    marginBottom: 10,
    
  },
  column:{
    flexDirection:"column",
    marginBottom:10,
    width:"50%"
  }
});

export default ProfileScreen;
