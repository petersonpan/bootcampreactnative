import React from "react";
import {LogBox} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import SignInScreen from "./src/screens/SignInScreen";
import SignUpScreen from "./src/screens/SignUpScreen";
import HomeScreen from "./src/screens/HomeScreen";
import ProfileScreen from "./src/screens/ProfileScreen";
import NotificationScreen from "./src/screens/NotificationScreen";
import PostScreen from "./src/screens/PostScreen";

import { AuthContext, AuthProvider } from "./src/providers/AuthProvider";
//import firebase from "firebase/compat/app";
import * as firebase from "firebase";
import {
  Entypo,
  AntDesign,
  Ionicons,
  FontAwesome,
  MaterialCommunityIcons,
} from "@expo/vector-icons";


const firebaseConfig = {
    apiKey: "AIzaSyAwCsrvvbevpiEV6Myt0y-HaIExF32md7w",
    authDomain: "testmind-d08b6.firebaseapp.com",
    projectId: "testmind-d08b6",
    storageBucket: "testmind-d08b6.appspot.com",
    messagingSenderId: "844683233224",
    appId: "1:844683233224:web:1e2220024ed29cb28762fd"
};


if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app();
}

const AuthStack = createStackNavigator();
const HomeTab = createMaterialBottomTabNavigator();
const AppDrawer = createDrawerNavigator();

const PostStack = createStackNavigator();
LogBox.ignoreLogs(['Setting a timer']);

const PostStackScreen = () => {
  return (
    <PostStack.Navigator initialRouteName="Home">
      <PostStack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <PostStack.Screen
        name="Post"
        component={PostScreen}
        options={{ headerShown: false }}
      />
    </PostStack.Navigator>
  );
};

const HomeTabScreen = () => {
  return (
    <HomeTab.Navigator>
      <HomeTab.Screen
        name="Post2"
        component={PostStackScreen}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Entypo name="home" color="white" size={26} />
            ) : (
              <AntDesign name="home" color="white" size={22} />
            ),
        }}
      />
      <HomeTab.Screen
        name="Notification"
        component={NotificationScreen}
        options={{
          tabBarLabel: "Notifications",
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Ionicons name="ios-notifications" size={26} color="white" />
            ) : (
              <Ionicons
                name="ios-notifications-outline"
                size={22}
                color="white"
              />
            ),
        }}
      />
    </HomeTab.Navigator>
  );
};

const AppDrawerScreen = () => {
  return (
    <AppDrawer.Navigator initialRouteName="Home">
      <AppDrawer.Screen
        name="Home"
        options={{
          headerShown: false,
          drawerIcon: ({ color, size }) => (
            <FontAwesome name="home" color="blue" size={22} />
          ),
        }}
        component={HomeTabScreen}
      />
      <AppDrawer.Screen
        name="Profile"
        options={{
          headerShown: false,

          drawerIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="face-profile"
              size={22}
              color="blue"
            />
          ),
        }}
        component={ProfileScreen}
      />
    </AppDrawer.Navigator>
  );
};

const AuthStackScreen = () => {
  return (
    <AuthStack.Navigator initialRouteName="SignIn">
      <AuthStack.Screen
        name="SignIn"
        component={SignInScreen}
        options={{ headerShown: false }}
      />
      <AuthStack.Screen
        name="SignUp"
        component={SignUpScreen}
        options={{ headerShown: false }}
      />
    </AuthStack.Navigator>
  );
};

function App() {
  return (
    <AuthProvider>
      <AuthContext.Consumer>
        {(auth) => (
          <NavigationContainer>
            {auth.IsLoggedIn ? <AppDrawerScreen /> : <AuthStackScreen />}
          </NavigationContainer>
        )}
      </AuthContext.Consumer>
    </AuthProvider>
  );
}

export default App;
