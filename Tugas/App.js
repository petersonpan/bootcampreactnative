import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Login from './Tugas-13/login';
import AboutScreen from './Tugas-13/about';
import RestApi from './Tugas-14/restApi';
import Tugas15 from './Tugas-15/index';
export default function App() {
  return (
    <>
      <StatusBar style="auto" translucent={false} />
      <Tugas15/>
    </> 
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
