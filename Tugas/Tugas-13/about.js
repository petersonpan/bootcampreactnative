import React,{useState} from "react";
import {
     StyleSheet, Text, TextInput, View ,Image,TouchableOpacity,SafeAreaView,StatusBar,Card} from "react-native";

import { MaterialCommunityIcons } from "@expo/vector-icons";

export default function AboutScreen(){
    // const [defaultStyle, setDefaultStyle] = useState(true);
    return(
    <SafeAreaView>
      <View style={styles.container}>
        <Text style={styles.style2}>
          Tentang Saya
        </Text>
        <MaterialCommunityIcons name="account" size={100} style={{marginTop:30,marginBottom:30,}} color="#B3B3B3" />
        <Text style={styles.style2}>
          Peterson Pandiangan
        </Text>
        <Text style={styles.style2}>
          React Native Developer
        </Text>
        <View style={styles.Card}>
            <View style={styles.CardContent}>
                <Text>Portofolio</Text>
                <View style={styles.itemSeparator}/>
                <View>
                <MaterialCommunityIcons name="github" size={100} style={{marginTop:30,marginBottom:30,}} color="#B3B3B3" />
                <Text>@petersonpan</Text>
                </View>
                <View>
                <MaterialCommunityIcons name="gitlab" size={100} style={{marginTop:30,marginBottom:30,}} color="#B3B3B3" />
                <Text>@petersonpan</Text>
                </View>
            </View>
        </View>
      </View>
    </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
      padding:20,
      alignItems:"center",
    },
    style1: {
      fontSize: 20,
    },
    style2: {
      fontSize: 16,
      color: '#0e0e0e',
      padding:5
    },
    Card:{
        flexDirection:"column",
		paddingVertical:15,
		paddingBottom:5,
		paddingTop:10,
        width:"100%",
        borderRadius:6,
        elevation:3,
        backgroundColor:"#fff",
        shadowOffset:{width:1,height:1},
        shadowColor:'#333',
        shadowOpacity:0.3,
        shadowRadius:2,
        marginHorizontal:4,
        marginVertical:6,
    },
    CardContent:{
        marginHorizontal:20,
        marginVertical:10,
    },
    itemSeparator:{
		height:1,
		backgroundColor:"#E5E5E5",
		width:"100%",
		alignSelf:"flex-end",
        marginTop:15,
        marginBottom:15
	},
  });