import React from 'react'
import { View, Text, TouchableOpacity,Button,StyleSheet } from 'react-native'
import { useNavigation } from '@react-navigation/native'

const Login = () => {
    const navigation = useNavigation();
    return (
        <View style={styles.contain}>
            <TouchableOpacity>
                <Button onPress={()=>navigation.navigate('OurDrawwer',
                   {
                       screen : 'Dashboard',params:{
                           screen : 'Dashboard'
                       }
                   } 
                )} 
                title="Move to Our App Screen"/>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.item}>
                <Button onPress={()=>navigation.navigate('OurApp')} title="Move to Our App Screen"/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item}>
                <Button style={{color:"red"}} onPress={()=>navigation.navigate('Dashboard')} title="Move to Dashboard Screen"/>
            </TouchableOpacity> */}
        </View>
    )
}
export default Login

const styles = StyleSheet.create({
    contain:{
        flex:1,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
        paddingLeft:15,
        paddingRight:15
    },
    item:{
        width:'50%',
        marginLeft:10,
        marginRight:10
        
    }
});