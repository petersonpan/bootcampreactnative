import React from 'react';
import Login from '../Screen/Login'
import Signup from '../Screen/Signup';
import Dashboard from '../Screen/Dashboard';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import AddScreen from '../Screen/AddScreen';
import ProjectScreen from '../Screen/ProjectScreen';
import SkillProject from '../Screen/SkillProject';
import About from '../Screen/About';
import Setting from '../Screen/Setting';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen  name="Login" component={Login} />
        <Stack.Screen  name="Dashboard" component={Dashboard} />
        <Stack.Screen  name="OurApp" component={OurApp}/>
        <Stack.Screen  name="OurDrawwer" component={OurDrawwer}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const OurApp = () => (
        <Tab.Navigator>
            <Tab.Screen name ="About" component={About} />
            <Tab.Screen name ="Your Skill Project" component={SkillProject} />
            <Tab.Screen name ="Add Our Skill" component={AddScreen} />            
        </Tab.Navigator>
)

const OurDrawwer =()=> (
    <Drawer.Navigator>
    <Drawer.Screen component={OurApp} name="Our App" />    
    <Drawer.Screen name="Dashboard" component={Dashboard} />
    <Drawer.Screen name="Our Project App" component={ProjectScreen} />
    <Drawer.Screen name="Our Setting App" component={Setting} />
  </Drawer.Navigator>
  )