console.log("Bentuk Persegi 8 x 4");
var string="";
for(var row=0;row<4;row++){
    for(var column=0;column<8;column++){
        string+="*";
    }
    string +="\n";
}
console.log(string);

console.log("Bentuk Tangga");
let n = 5;
let str = "";
for (let i = 1; i <= n; i++) {
  for (let j = 0; j < i; j++) {
    str += "*";
  }
  str += "\n";
}
console.log(str);

console.log("bentuk catur");
var Col=8;
var Row=8;
for(var i=0;i<Row;i++){
    var bentuk = i % 2 === 1 ? ' ' : '#';
    var kalimat = '';
    for(var j=0;j<Col;j++){
        bentuk = bentuk == '#' ? ' ' : '#';
        kalimat += bentuk;
    }
    console.log(kalimat);
}

console.log("\n\n Looping Pertama \n\n");

for(var index = 2;index<=20;index+=2){
   console.log(index + " I Love Coding");
}

console.log("\n\n Looping Kedua \n\n");

for(var index = 20;index>=2;index-=2){
    console.log(index + " I Love Coding");
}

console.log("\n\n Looping Ketiga \n\n");
for(var idx = 1;idx<=20;idx++){
    if(idx == 3){
        console.log(idx+" - I Love Coding");
        continue;
    }
    if(idx % 2 == 1 ){
        console.log(idx+" - Santai");
    }else if(idx % 3 == 0){
        console.log(idx+" - I Love Coding");
    }else if(idx % 2 == 0){
        console.log(idx+" - Berkualitas");
    }
}