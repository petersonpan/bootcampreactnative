function Range(startNumber,finishNumber){
	if(startNumber == finishNumber) return [startNumber];
	return [startNumber,...Range(startNumber+1,finishNumber)];
}

function RangeWithStep(startNumber,finishNumber,step){
	var result = [];
	if((step > 0 && startNumber >= finishNumber) || (step < 0 && startNumber <= finishNumber)){
		return [];
	}
	for (var i = startNumber; step > 0 ? i < finishNumber : i > finishNumber; i+=step) {
		result.push(i);
	}
	return result;
}




var input = [
    ["0001", "Roman", "Bandar Lampung", "21/05/1929", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "01/01/1992", "Bermain Gitar"],
    ["0003", "Wimon", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Sentaya", "Martapura", "6/4/1990", "Berkebun"]
  ];

 var desc = [
 	"Nomor ID","Nama Lengkap","TTL","Hobi"
 ]

function dataHandling(){

   for(var i=0;i<input.length;i++){
   	var data = input[i].length;
   	 for(var j=0;j<data;j++){
   	 	if(desc[j] == "TTL"){
   	 		console.log(desc[j]+":"+input[i][2]+" "+input[i][3]);
   	 		continue;
   	 	}
   	 	if(desc[j] == "Hobi"){
   	 		console.log(desc[j]+":"+input[i][4]);
   	 		continue;
   	 	}
   	 	if(desc[j] == undefined){
   	 		continue;
   	 	}
   	 	console.log(desc[j]+":"+input[i][j]);
   	 	
   	 }
   	 console.log('\n');	
   }
}

function sum(startNum,finishNum,step){
	var jum = 0;
	for(var idx=startNum;idx<=finishNum;idx+=step){
		jum +=idx;
	}
	return jum;
}

function balikKata(str){
	var kalimat="";
	for(var index = str.length-1;index >= 0;index--){
		kalimat+=str[index];
	}
	return kalimat;
}

var inPut = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(inPut){
  
  	// inPut[1]="Roman Alamsyah  Elsharawy";
  	// inPut[2]="Provinsi Bandar Lampung";
  	// inPut.pop();
  	// inPut.push("Pria", "SMA Internasional Metro");
  	inPut.splice(1,4,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung","21/05/1989", "Pria", "SMA Internasional Metro");
  	//input.splice(2,1,"Provinsi Bandar Lampung");
  	
  	var month=inPut[3].split("/");
  	var bulan;
  	var monthsort;
  	switch(month[1]) {
    case '01':    bulan = " Januari "; break; 
    case '02':    bulan = " Februari "; break; 
    case '03':    bulan = " Maret "; break; 
    case '04':    bulan = " April "; break; 
    case '05':    bulan =" Mei "; break; 
    case '06':    bulan =" Juni "; break; 
    case '07':    bulan =" Juli "; break; 
    case '08':    bulan =" Agustus "; break; 
    case '09':    bulan =" September "; break; 
    case '10':    bulan =" Oktober "; break; 
    case '11':    bulan =" November "; break; 
    case '12':    bulan =" Desember "; break; 
    default:   bulan ="Tidak terjadi apa-apa"; }

  console.log(inPut+"\n")
  console.log(bulan+"\n");
  console.log(month.join("-"));
  monthsort=month.sort(function(value1,value2){return value2 - value1 });
  console.log(monthsort+
  	"\n");
  console.log(inPut[1].slice(0,15));


}

console.log(Range(0,3));
console.log(RangeWithStep(0,6,2));
dataHandling();
console.log("Jumlah:"+sum(0,7,2));
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers"+"\n")) // srebnaS ma I
console.log("\n");
dataHandling2(inPut);