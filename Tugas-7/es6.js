"use strict"

const golden = () => "this is golden!!"; 

console.log(golden());


function newFunction(firstName,lastName){
	return {
		firstName:firstName,
		lastName:lastName,
		fullName(){
			return this.firstName + " "+ this.lastName;
		}
	}
}


const person = newFunction("Peter","parker");
console.log(person);
console.log(person.fullName());

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName,lastName,destination,occupation,spell} = newObject;

console.log(firstName, lastName, destination, occupation,spell);

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west,...east];
console.log(combined);

const planet = "earth"
const view = "glass"
var before = `Lorem  ${view}  dolor sit amet,consectetur adipiscing elit, ${planet} do eiusmod temporincididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
console.log(before);

