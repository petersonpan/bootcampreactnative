
function replaceCharacter(str,from,to){
  var out = "";
  if(to == undefined){
    to = '';
  }
  for(var i = 0;i < str.length; i++){
    if(str.charAt(i) === from){
      out += to;
    }else{
      out += str.charAt(i);
    }
  }
  return out;
}

var studentData = [
	[2, "John Duro", 60],
	[4, "Robin Ackerman", 100],
	[1, "Jaeger Marimo", 60],
	[6, "Zoro", 80],
	[5, "Zenitsu", 80],
	[3, "Patrick Zala", 90],
];


console.log(studentData.sort((a,b) => a[2] - b[2]));
var sentence = "Naik delman istimewa ku duduk di muka";
var result = replaceCharacter(sentence,"a");
console.log(result);