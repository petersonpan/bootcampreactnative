'use strict'
const readBooks = require('./promise.js');
const books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
];
//recursive 
function startReading(remainingTime, books, index) {
    readBooks(remainingTime, books[index], function (time) {
        const nextBook = index + 1;
        if (nextBook < books.length) {
            startReading(time, books, nextBook);
        }
    });
}

startReading(10000, books, 0);