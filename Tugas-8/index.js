const readBooks = require('./callback.js');

var books = [
	{ name: 'LOTR', timespent: 3000 },
	{ name: 'Fidas', timespent: 2000 },
	{ name: 'Kalkulus', timespent: 4000 },

];


function startReading(remainingTime, books, index) {
	readBooks(remainingTime, books[index], function (time) {
		const nextBook = index + 1;
		if (nextBook < books.length) {
			startReading(time, books, nextBook);
		}
	});
}

startReading(10000, books, 0);