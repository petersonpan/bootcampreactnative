'use strict'
function readBook(time, book, callback) {
	console.log(`saya membaca ${book.name}`);
	let sisaWaktu = 0;
	setTimeout(() => {

		if (time >= book.timeSpent) {
			sisaWaktu = time - book.timeSpent;
			console.log(`saya sudah membaca ${book.name}, sisa wwaktu saya ${sisaWaktu}`);
			callback(sisaWaktu);
		} else {
			console.log(`Waktu saya habis`);
			callback(time);
		}
	}, book.timeSpent);
}

module.exports = readBook;