import React from "react";
import {
     StyleSheet, Text, TextInput, View ,Image,TouchableOpacity
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";


export default function Login(){
 return(
	<View style={styles.container}>
		<Image
				source={require("../assets/sanbersLogo.png")}
				style={styles.logo}
			/>

	<Text style={styles.title}>Login</Text>
	<View style={styles.form}>
		<View style={styles.formField}>
			<Text style={styles.formTitle}>Username/Email</Text>
			<View style={styles.formTextInputContainer}>
					<TextInput style={styles.formTextInput}/>
			</View>
			<Text style={styles.error}>fill your username</Text>
		</View>
		<View style={styles.formField}>
			<Text style={styles.formTitle}>Password</Text>
			<View style={styles.formTextInputContainer}>
				<TextInput
					style={styles.formTextInput}
					secureTextEntry                        />
					<MaterialCommunityIcons name="eye" size={24} color="#B3B3B3" />
			</View>
			<Text style={styles.error}>fill your password</Text>
		</View>
		<View>
		<TouchableOpacity style={styles.button}>
		<Text style={styles.buttonTitle}>Login</Text>
	</TouchableOpacity>
		<Text style={{textAlign:"center"}}>Atau</Text>
		<TouchableOpacity style={styles.buttonSignUp}>
		<Text style={styles.buttonTitleSignUp}>Masuk</Text>
	</TouchableOpacity>
		</View>
	</View>
</View>
 );
}

const styles = StyleSheet.create({
	container: {
		padding: 20,
	},
	logo: {
		alignSelf: "center",
	},
	title: {
		marginTop: 40,
		marginBottom: 20,
		fontSize: 20,
		color:"#003366",
		textAlign:"center",
	},

	formTitle: {
		fontSize: 14,
		fontWeight: "bold",
		marginBottom: 8,
	},
	formField: {
		marginBottom: 10,
	},
	formTextInputContainer: {
		borderWidth: 1,
		borderColor: "#B3B3B3",
		borderRadius: 9,
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: 15,
		paddingVertical: 10,
	},
	formTextInput: {
		flex: 1,
	},
	error: {
		marginTop: 5,
		fontSize: 10,
		color: "#FF3535",
	},
	button: {
		backgroundColor: "#003366",
		borderRadius: 16,
		alignItems: "center",
		justifyContent: "center",
		paddingVertical: 14,
		marginVertical: 10,
	},
	buttonTitle: {
		fontSize: 18,
		fontWeight: "700",
		color: "white",
	},
	buttonSignUp: {
		backgroundColor: "#ffffff",
		borderRadius: 16,
		alignItems: "center",
		justifyContent: "center",
		paddingVertical: 14,
		marginVertical: 10,
		borderRadius:10,
		borderColor:"#EFEFEF"
	},
	buttonTitleSignUp: {
		fontSize: 18,
		fontWeight: "700",
		color: "#3EC6FF",
	},
	

});
