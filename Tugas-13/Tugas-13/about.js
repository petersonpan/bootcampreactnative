import React from "react";
import {
     StyleSheet, Text, View ,Image,TouchableOpacity,SafeAreaView,ScrollView,StatusBar} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default function AboutScreen(){
    return(
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
            <Text style={styles.style2}>
              Tentang Saya
            </Text>
            <MaterialCommunityIcons name="account" size={100} style={{marginTop:30,marginBottom:30,borderRadius:50}} color="#B3B3B3" />
            <Text style={styles.style2}>
              Peterson Pandiangan
            </Text>
            <Text style={styles.style2}>
              React Native Developer
            </Text>
            <View style={{backgroundColor:"#EEE",padding:15,}}>
            <View style={{alignSelf:"flex-start",paddingBottom:10}}>
                  <Text>Portofolio</Text>
            </View>
            <View style={styles.itemSeparator}/>
            <View style={styles.Card}>
                <View style={styles.CardContent}>
                      <MaterialCommunityIcons name="github" size={100}  color="#B3B3B3" />
                      <Text>@petersonpan</Text>
                </View>
                <View style={styles.CardContent}>
                    <MaterialCommunityIcons name="gitlab" size={100} color="#B3B3B3" />
                    <Text>@petersonpandi</Text>
                </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
      padding:20,
      alignItems:"center",
    },
    style1: {
      fontSize: 20,
    },
    style2: {
      fontSize: 16,
      color: '#0e0e0e',
      padding:5
    },
    Card:{
        alignContent:"center",
        borderRadius:6,
        flexDirection:"row",
        justifyContent:"space-around",
        shadowOffset:{width:1,height:1},
        shadowColor:'#333',
        shadowOpacity:0.3,
        shadowRadius:2,
        width:"100%",
    },
    CardContent:{
      backgroundColor:"#EEE",
    },
    itemSeparator:{
      alignSelf:"flex-start",
      height:1,
		  backgroundColor:"#eee",
		  width:"50%",
      marginBottom:15
	  },
  });