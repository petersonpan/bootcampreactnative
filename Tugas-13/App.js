import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Login from './Tugas-13/login';
import AboutScreen from './Tugas-13/about';
export default function App() {
  return (
    <>
      <StatusBar style="auto" translucent={false} />
      <AboutScreen/>
    </> 
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
