import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
//import Telegram from './component/telegram';
import RestApi from './component/restApi';

export default function App() {
  return (
    <RestApi/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
